#define _CRT_SECURE_NO_WARNINGS

//
//#include <iostream>
//
//using std::cout;
//using std::cin;
//using std::endl;
//
//int main()
//{
//
//
//	return 0;
//}

//#include <stdio.h>
//int i;
//void prt()
//{
//	for (i = 5; i < 8; i++)
//		printf("%c", '*');
//	printf("\t");
//} 
//
//int main()
//{
//	for (i = 5; i <= 8; i++)
//		prt();
//	return 0;
//}


//#include <stdio.h>
//
//int main()
//{
//	float f = 10.15f;
//
//	printf("%.1f", f);
//
//	return 0;
////}
//
//#include <stdio.h>
//
//int main()
//{
//    int n = 0;
//    scanf("%d", &n);
//
//    int NegaNum = 0;
//    int PosiNum = 0;
//    int k = 0;
//    int tmp = 0;
//    for (int i = 0; i < n; i++)
//    {
//        scanf("%d", &tmp);
//        if (tmp < 0)
//        {
//            NegaNum++;
//        }
//
//        if (tmp > 0)
//        {
//            PosiNum += tmp;
//            k++;
//        }
//    }
//    float er = 0.0;
//
//    if (k == 0)
//        er = (float)PosiNum;
//    else
//        er = (float)((float)PosiNum / k);
//
//    printf("%d %.1f", NegaNum, (float)er);
//
//    return 0;
//}


#include <iostream>

using namespace std;

class date
{
public:
	date(int year = 2004, int month = 1, int day = 29)
	{
		_year = year;
		_month = month;
		_day = day;
	}

	~date()
	{
		_year = 0;
		_month = 0;
		_day = 0;
	}

	date(const date& ld)
	{
		_year = ld._year;
		_month = ld._month;
		_day = ld._day;
	}

	void Print()
	{
		cout << _year << ' ' << _month << ' ' << _day << endl;
	}

	bool operator==(const date& d)
	{
		return _year == d._year &&
			_month == d._month &&
			_day == d._day;
	}

	bool operator > (const date& d)
	{
		if (_year > d._year)
		{
			return true;
		}
		else if (_year == d._year && _month > d._month)
		{
			return true;
		}
		else if (_year == d._year && _month == d._month && _day > d._day)
		{
			return true;
		}

		return false;
	}

	bool operator>=(const date& d)
	{
		return *this > d || *this == d;
	}

	int GetMonthDay(int year, int month)
	{
		int arr[] = { 0,31,28,31,30,31,30,31,31,30,31,30,31 };
		if (month == 2 && ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0))
		{
			return 29;
		}

		return arr[month];
	}

	date& operator += (int day)
	{
		_day += day;
		while (_day > GetMonthDay(_year, _month))
		{
			_day -= GetMonthDay(_year, _month);
			_month++;

			if (_month == 13)
			{
				_year++;
				_month = 1;
			}
		}
		
		return *this;
	}

	date operator + (int day)
	{
		date ret(*this);
		ret += day;

		return ret;
	}

	// 日期-天数
	date operator-(int day)
	{
		date ret(*this);
		ret -= day;

		return ret;
	}

	// 日期-=天数
	date& operator-=(int day)
	{
		if (_day < day)
		{
			if (_month == 1)
			{
				_day += GetMonthDay(--(_year), 12);
				_month = 12;
				_day -= day;
			}
			else
			{
				_day += GetMonthDay(_year, --(_month));
				_day -= day;
			}
		}
		else
		{
			_day -= day;
		}

		return *this;
	}

	// 前置++
	date& operator++();

	// 后置++
	date operator++(int);

	// 后置--
	date operator--(int);

	// 前置--
	date& operator--();

	// <运算符重载
	bool operator < (const date& d)
	{
		if (_year < d._year)
		{
			return true;
		}
		else if (_year == d._year && _month < d._month)
		{
			return true;
		}
		else if (_year == d._year && _month == d._month && _day < d._day)
		{
			return true;
		}

		return false;
	}

	// <=运算符重载
	bool operator <= (const date& d)
	{
		return (*this) == d || (*this) < d;
	}

	// !=运算符重载
	bool operator != (const date& d)
	{
		return !((*this) == d);
	}

	int GetYearDay(int year)
	{
		if ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0)
		{
			return 366;
		}

		return 365;
	}

	// 日期-日期 返回天数
	int operator-(const date& d)
	{
		//int MaxYear = _year;
		//if (MaxYear < d._year)
		//	MaxYear = d._day;

		//int DayNum = 0;
		//--MaxYear;
		//while (MaxYear != _year)
		//{
		//	DayNum += GetYearDay(MaxYear);
		//	--MaxYear;
		//}
		//DayNum += GetYearDay(MaxYear);

		int DayNum1 = 0;
		int DayNum2 = 0;

		DayNum1 += _day;
		DayNum2 += d._day;

		int tmpm1 = _month;
		while (--tmpm1)
		{
			DayNum1 += GetMonthDay(_year, _month);
		}

		int tmpm2 = d._month;
		while (--tmpm2)
		{
			DayNum2 += GetMonthDay(d._year, d._month);
		}

		int maxy;
		if (_year > d._year)
		{
			maxy = _year;
			--maxy;
			while (maxy != d._year)
			{
				DayNum1 += GetYearDay(maxy);
				--maxy;
			}
			DayNum1 += GetYearDay(maxy);
		}
		else if (_year < d._year)
		{
			maxy = d._year;
			--maxy;
			while (maxy != _year)
			{
				DayNum2 += GetYearDay(maxy);
				--maxy;
			}
			DayNum2 += GetYearDay(maxy);
		}

		return DayNum1 - DayNum2 > 0 ? DayNum1 - DayNum2 : DayNum2 - DayNum1;
	}

private:
	int _year;
	int _month;
	int _day;
};

int main()
{
	//date d1;
	//date d2(2023, 1, 20);
	//date d3(d2);

	//cout << (d1 == d2) << endl;
	//cout << (d2 >= d1) << endl;
	//cout << (d2 == d3) << endl;

	//date d4;
	//date d5 = d4 + 2;
	//d4.Print();
	//d5.Print();
	//d4 += 2;
	//d4.Print();

	//d1.Print();
	//d2.Print();
	//d3.Print();

	//date d2(2023, 1, 20);
	//date d3 = d2 - 30;

	//d2.Print();
	//d3.Print();

	//d2 -= 30;
	//d2.Print();
	//d2 -= 10;
	//d2.Print();

	date d1(2023, 1, 1);
	date d2(2020, 4, 25);

	int gap = d1 - d2;

	cout << gap << endl;

	return 0;
}
